#include<stdio.h>
#include<stdlib.h>
#include<time.h>
void help()
{
	printf("帮助信息:\n");
	printf(" 您需要输入命令代号来进行操作，且\n");
	printf("一年级的题目为不超过有十位的加减法:\n");
	printf("二年级题目为不超过百位的乘除法:\n");
	printf("三年级题目为不超过百位的加减乘除混合题目。\n");
	printf(" \n");
}
void menu()
{
	printf("操作列表: \n");
	printf("1)一年级 \n ");
    printf("2）二年级 \n ");
	printf("3）三年级\n ");
	printf("4）帮助 \n ");
    printf("5）退出程序\n");
	printf("请输入操作\n");
	printf("执行中...\n");
}
void error(int d)
{
	if(d<1||d>5)
	printf("Error!!!\n");
	printf("错误指令操作，请重新输入\n");
	printf("\n");
}
void Firstgrade()
{
	int D;
	printf("现在是一年级题目:\n");
	printf("请输入生成个数:\n");
	scanf("%d",&D);
	printf("/*----- 一年级 -----*/\n");
	int a,b,c;
	srand((unsigned)time(NULL));
	for(int i=0;i<D;i++)
	{
		a = rand()%10;b = rand()%10;c = rand()%2;
		if(c==0){
			printf("%d + %d = %d\n",a,b,a+b);
		}
		else{
			printf("%d - %d = %d\n",a,b,a-b);
		}
	}
}
void Secondgrade()
{
	int D;
	printf("现在是二年级题目:\n");
	printf("请输入生成个数:\n");
	scanf("%d",&D);
	printf("/*----- 二年级 -----*/\n");
	int a,b,c;
	srand((unsigned)time(NULL));
	for(int i=0;i<D;i++)
	{
		a = rand()%100;
		b = rand()%100;
		c = rand()%2;
		if(c == 0)
		{
			printf("%d * %d = %g\n",a,b,(double)a*b);
		}
		else
		{
			printf("%d / %d = %g\n",a,b,(double)a/b+1);
		}
	}
}
void Thirdgrade()
{
	int D;
	printf("现在是三年级题目:\n");
	printf("请输入生成个数:\n");
	scanf("%d",&D);
	printf("/*----- 三年级 -----*/\n");
	int a,b,c,e;
	char d[4]={'+','-','*','/'},t[4]={'+','-','*','/'},y,u;
	srand((unsigned)time(NULL));
	for(int i=0;i<D;i++)
	{
		y=d[rand()%4];
		u=t[rand()%4];
		b=rand()%100;
		if(y=='+')
		{
			switch(u)
			{
				case '+':
				c=rand()%100;e=rand()%100;
				printf("%d + %d + %d = %g\n",b,c,e,(double)b+c+e);
				break;
				case '-':
				c=rand()%100;e=rand()%100;
				printf("%d + %d - %d = %g\n",b,c,e,(double)b+c-e);
				break;
				case '*':
				c=rand()%100;e=rand()%100;
				printf("%d + %d * %d = %g\n",b,c,e,(double)b+c*e);
				case '/':
				c=rand()%100;e=rand()%100;
				printf("%d + %d 、 %d = %g\n",b,c,e,(double)b+c/e);
				break;
			}
		}
		if(y=='-')
		{
			switch(u)
			{
				case '+':
				c=rand()%100;e=rand()%100;
				printf("%d - %d + %d = %g\n",b,c,e,(double)b-c+e);
				break;
				case '-':
				c=rand()%100;e=rand()%100;
				printf("%d - %d - %d = %g\n",b,c,e,(double)b-c-e);
				break;
				case '*':
				c=rand()%100;e=rand()%100;
				printf("%d - %d * %d = %g\n",b,c,e,(double)b-c*e);
				break;
				case '/':
				c=rand()%100;e=rand()%100;
				printf("%d - %d / %d = %g\n",b,c,e,(double)b-c/e);
			}
		}
		if(y=='*')
		{
				switch(u)
			{
				case '+':
				c=rand()%100;e=rand()%100;
				printf("%d * %d + %d = %g\n",b,c,e,(double)b*c+e);
				break;
				case '-':
				c=rand()%100;e=rand()%100;
				printf("%d * %d - %d = %g\n",b,c,e,(double)b*c-e);
				break;
				case '*':
				c=rand()%100;e=rand()%100;
				printf("%d * %d * %d = %g\n",b,c,e,(double)b*c*e);
				break;
				case '/':
				c=rand()%100;e=rand()%100;
				printf("%d * %d / %d = %g\n",b,c,e,(double)b*c/e);
				break;
			}
		}
		 if(y=='/')
		{
				switch(u)
			{
				case '+':
				c=rand()%99+1;e=rand()%100;
				printf("%d / %d + %d = %g\n",b,c,e,(double)b/c+e);
				break;
				case '-':
				c=rand()%99+1;e=rand()%99+1;
				printf("%d / %d - %d = %g\n",b,c,e,(double)b/c-e);
				break;
				case '*':
				c=rand()%99+1;e=rand()%99+1;
				printf("%d / %d * %d = %g\n",b,c,e,(double)b/c*e);
				break;
				case '/':
				c=rand()%99+1;e=rand()%99+1;
				printf("%d / %d / %d = %g\n",b,c,e,(double)b/c/e);
				break;
			}
		}
   }
}
int main()
{
	int i,x;
	printf("========== 口算生成器 ========== \n");
	printf("欢迎使用口算生成器: \n");
	help();
	for(i=1;i<=100;i++)
	{
    	menu();
     	scanf("%d",&x);
     	printf("\n");
     	if(x==5)
     	break;
     	switch(x)
       	{
	       case 1:
	       	Firstgrade();
	         	printf("\n");
	        	break;
	       case 2:
	       	Secondgrade();
		        printf("\n");
		        break;
           case 3:
           	Thirdgrade();
		        printf("\n");
	        	break;
	        case 4:
	         	help();
	            break;
	            default:
                error(i);
             	break;
	     }
	        printf("\n");
    }
         	printf("\n");
            printf("程序结束, 欢迎下次使用任意键结束\n");
	        return 0;
}
